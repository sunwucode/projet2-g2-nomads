﻿using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class PrestationDAO : DAO
    {

        public List<PrestationEntity> GetAllPrestation()
        {
            List<PrestationEntity> result = new List<PrestationEntity>();
            // Preparer la command
            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.prestation ORDER BY date ASC;");

            try
            {
                // ouvrir la connection 
                cmd.Connection.Open();

                // Exécuter la command
                IDataReader dr = cmd.ExecuteReader();

                // Traiter le resultat
                while (dr.Read())
                {
                    result.Add(DataReaderToPrestationEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }


        public List<PrestationEntity> GetAllInscriptionPrestationByMasseur(int idMasseur)
        {
            List<PrestationEntity> result = new List<PrestationEntity>();
            // Preparer la command
            IDbCommand cmd = GetCommand(@"  SELECT *
                                            FROM nomads.prestation p
                                            JOIN nomads.inscription_prestation i on p.id_prestation = i.id_prestation
                                            JOIN nomads.masseur m on m.id_masseur = i.id_masseur
                                            WHERE m.id_masseur = @idmasseur AND p.date >= SYSDATE()
                                            ORDER by p.libelle_prestation;");
            cmd.Parameters.Add(new MySqlParameter("@idmasseur", idMasseur));

            try
            {
                // ouvrir la connection 
                cmd.Connection.Open();

                // Exécuter la command
                IDataReader dr = cmd.ExecuteReader();

                // Traiter le resultat
                while (dr.Read())
                {
                    result.Add(DataReaderToPrestationEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }

        public List<PrestationEntity> GetHistoriqueInscriptionByMasseur(int idMasseur)
        {
            List<PrestationEntity> result = new List<PrestationEntity>();
            // Preparer la command
            IDbCommand cmd = GetCommand(@"  SELECT *
                                            FROM nomads.prestation p
                                            JOIN nomads.inscription_prestation i on p.id_prestation = i.id_prestation
                                            JOIN nomads.masseur m on m.id_masseur = i.id_masseur
                                            WHERE m.id_masseur = @idmasseur AND p.date <= SYSDATE()
                                            ORDER by p.libelle_prestation;");
            cmd.Parameters.Add(new MySqlParameter("@idmasseur", idMasseur));

            try
            {
                // ouvrir la connection 
                cmd.Connection.Open();

                // Exécuter la command
                IDataReader dr = cmd.ExecuteReader();

                // Traiter le resultat
                while (dr.Read())
                {
                    result.Add(DataReaderToPrestationEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();
            }
            return result;
        }

        

        public List<InscriptionPrestationEntity> GetListAllInscriptionPrestationEntities()

        {
            List<InscriptionPrestationEntity> ListInscriptionPrestationEntity = new List<InscriptionPrestationEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand("SELECT * FROM nomads.inscription_prestation");

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    ListInscriptionPrestationEntity.Add(DataReaderToInscriptionPrestationEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return ListInscriptionPrestationEntity;
        }

        public List<InscriptionPrestationEntity> GetListHistoriqueInscriptionsByMasseur(int idMasseur)

        {
            List<InscriptionPrestationEntity> ListHistoriqueInscriptionPrestationEntity = new List<InscriptionPrestationEntity>();

            // 2 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"  SELECT * FROM nomads.inscription_prestation i
                                            JOIN nomads.prestation p
                                            WHERE id_masseur = @idmasseur AND date < SYSDATE() 
                                            ORDER BY p.libelle_prestation; ");

            cmd.Parameters.Add(new MySqlParameter("@idmasseur", idMasseur));

            try
            {
                // 3 - Ouvrir la connection
                cmd.Connection.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    ListHistoriqueInscriptionPrestationEntity.Add(DataReaderToInscriptionPrestationEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cmd.Connection.Close();
            }
            return ListHistoriqueInscriptionPrestationEntity;
        }
        public List<PrestationEntity> GetRecherche(string nom, int type)
        {
            List<PrestationEntity> result = new List<PrestationEntity>();
            IDbCommand cmd = GetCommand(@"  SELECT *
                                            FROM nomads.prestation p
                                            JOIN nomads.type_prestation t ON p.id_type_prestation = t.id_type_prestation
                                              
                                            WHERE p.libelle_prestation LIKE @nom
                                            AND (@idType = -1 OR t.id_type_prestation = @idType)

                                            ORDER by p.libelle_prestation

                                        ");

            cmd.Parameters.Add(new MySqlParameter("@nom", "%" + nom + "%"));
            cmd.Parameters.Add(new MySqlParameter("@idType", type));
            

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result.Add(DataReaderToPrestationEntity(dr));
                }

            }
            catch (Exception exc)
            {

                throw exc;
            } finally
            {
                cmd.Connection.Close();
            }
            return result;
        }

        public void DesinscriptionPrestation(int idMasseur, int idPrestation)
        {
            IDbCommand cmd = GetCommand(@"DELETE FROM nomads.inscription_prestation
                                            WHERE id_masseur = @idMasseur AND id_prestation = @idPrestation;");

            cmd.Parameters.Add(new MySqlParameter("@idMasseur", idMasseur));
            cmd.Parameters.Add(new MySqlParameter("@idPrestation", idPrestation));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public void InscriptionPrestation(InscriptionPrestationEntity nouvelleInscription)
        {
            IDbCommand cmd = GetCommand(@"INSERT INTO inscription_prestation
                                            (date_inscription, cout_mads, id_masseur, id_prestation)
                                            VALUES
                                            (@DateInscription, @CoutMads, @IdMasseur, @IdPrestation);");

            cmd.Parameters.Add(new MySqlParameter("@DateInscription", nouvelleInscription.DateInscription));
            cmd.Parameters.Add(new MySqlParameter("@CoutMads", nouvelleInscription.CoutMads));
            cmd.Parameters.Add(new MySqlParameter("@IdMasseur", nouvelleInscription.IdMasseur));
            cmd.Parameters.Add(new MySqlParameter("@IdPrestation", nouvelleInscription.IdPrestation));

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        public void AjouterTournee(PrestationEntity nouvelleTournee)
        {
            IDbCommand cmd = GetCommand (@"INSERT INTO prestation
                                            ( libelle_prestation, date, duree, masseur_min, cout_mads, 
                                            date_creation, adresse_prestation, id_partenaire, id_type_prestation, id_ville, masseur_max)
                                            VALUES
                                            (@libelle, @date, @duree, @masseurMin, @coutMads, 
                                            @dateCreation, @adressePrestation, @idPartenaire, @idTypePrestation, @idVille, @masseurMax);
                                            SELECT LAST_INSERT_ID();");

            cmd.Parameters.Add(new MySqlParameter("@libelle", nouvelleTournee.Libelle));
            cmd.Parameters.Add(new MySqlParameter("@date", nouvelleTournee.Date));
            cmd.Parameters.Add(new MySqlParameter("@duree", nouvelleTournee.Duree));
            cmd.Parameters.Add(new MySqlParameter("@masseurMin", nouvelleTournee.MasseurMin));
            cmd.Parameters.Add(new MySqlParameter("@coutMads", nouvelleTournee.CoutMads));
            cmd.Parameters.Add(new MySqlParameter("@dateCreation", nouvelleTournee.DateCreation));
            cmd.Parameters.Add(new MySqlParameter("@adressePrestation", nouvelleTournee.Adresse));
            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", nouvelleTournee.IdPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@idTypePrestation", nouvelleTournee.IdTypePrestation));
            cmd.Parameters.Add(new MySqlParameter("@idVille", nouvelleTournee.IdVille));
            cmd.Parameters.Add(new MySqlParameter("@masseurMax", nouvelleTournee.MasseurMax));


            try
            {
                cmd.Connection.Open();
                nouvelleTournee.IdPrestation = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public PrestationEntity GetPrestationById(int id)
        {
            PrestationEntity result = null;
            // preparer la commande
            IDbCommand cmd = GetCommand(@"SELECT * 
                                        FROM prestation
                                        WHERE id_prestation = @id");
            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // Ouvrir la connection 
                cmd.Connection.Open();

                //Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToPrestationEntity(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }
        private InscriptionPrestationEntity DataReaderToInscriptionPrestationEntity(IDataReader dr)
        {

            InscriptionPrestationEntity InscriptionPrestationEntity = new InscriptionPrestationEntity();

            InscriptionPrestationEntity.IdInscription = dr.GetInt32(dr.GetOrdinal("id_inscription_prestation"));
            InscriptionPrestationEntity.DateInscription = dr.GetDateTime(dr.GetOrdinal("date_inscription"));
            if (!dr.IsDBNull(dr.GetOrdinal("date_validation")))                 // Date de validation peut être null
            {
                InscriptionPrestationEntity.DateValidation = dr.GetDateTime(dr.GetOrdinal("date_validation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_desistement")))                // Date de désistement peut être null
            {
                InscriptionPrestationEntity.DateDesistement = dr.GetDateTime(dr.GetOrdinal("date_desistement"));
            }
            InscriptionPrestationEntity.CoutMads = dr.GetInt32(dr.GetOrdinal("cout_mads"));
            if (!dr.IsDBNull(dr.GetOrdinal("libelle_fb_masseur")))              // Libelle feedback peut être null
            {
                InscriptionPrestationEntity.LibelleFbMasseur = dr.GetString(dr.GetOrdinal("libelle_fb_masseur"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_fb_masseur")))                 // Date feedback masseur peut être null
            {
                InscriptionPrestationEntity.DateFbMasseur = dr.GetDateTime(dr.GetOrdinal("date_fb_masseur"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("montant_presta_intervention")))     // Montant prestation peut être null
            {
                InscriptionPrestationEntity.MontantPrestaIntervention = dr.GetDouble(dr.GetOrdinal("montant_presta_intervention"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_facture")))                    // Date facture peut être null
            {
                InscriptionPrestationEntity.DateFacture = dr.GetDateTime(dr.GetOrdinal("date_facture"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("note_feedback")))                  // Note feedback peut être null
            {
                InscriptionPrestationEntity.NoteFeedback = dr.GetInt32(dr.GetOrdinal("note_feedback"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("nombre_clients")))                 // Nombre clients peut être null
            {
                InscriptionPrestationEntity.NombreClients = dr.GetInt32(dr.GetOrdinal("nombre_clients"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("chiffre_affaires")))               // Date d'annulation peut être null
            {
                InscriptionPrestationEntity.ChiffreAffaires = dr.GetDouble(dr.GetOrdinal("chiffre_affaires"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_reservation")))               // Date de réservation peut être null
            {
                InscriptionPrestationEntity.DateReservation = dr.GetDateTime(dr.GetOrdinal("date_reservation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_retrait")))                   // Date de retrait peut être null
            {
                InscriptionPrestationEntity.DateRetrait = dr.GetDateTime(dr.GetOrdinal("date_retrait"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("date_retour")))                    // Date de retour peut être null
            {
                InscriptionPrestationEntity.DateRetour = dr.GetDateTime(dr.GetOrdinal("date_retour"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cout_mads_arrhes")))               // Cout mads arrhes peut être null
            {
                InscriptionPrestationEntity.CoutMadsArrhes = dr.GetInt32(dr.GetOrdinal("cout_mads_arrhes"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("id_type_penalite")))               // ID Type Penalite peut être null
            {
                InscriptionPrestationEntity.TypePenalite = dr.GetInt32(dr.GetOrdinal("id_type_penalite"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("id_materiel")))                    // ID Matéirel peut être null
            {
                InscriptionPrestationEntity.IdMateriel = dr.GetInt32(dr.GetOrdinal("id_materiel"));
            }
            InscriptionPrestationEntity.IdMasseur = dr.GetInt32(dr.GetOrdinal("id_masseur"));
            InscriptionPrestationEntity.IdPrestation = dr.GetInt32(dr.GetOrdinal("id_prestation"));


            return InscriptionPrestationEntity;
        }

        private PrestationEntity DataReaderToPrestationEntity(IDataReader dr)
        {
            PrestationEntity prestation = new PrestationEntity();

/*
id_prestation int AI PK
libelle_prestation varchar(255)
date date
duree time
masseur_min int
masseur_max int
cout_mads int
date_creation date
date_annulation date
adresse_prestation varchar(255)
libelle_fb_part varchar(255) 
date_fb_part date
montant_devis double
date_devis date
montant_facture double
note_feedback int
id_partenaire int
id_type_recurrence int
id_type_prestation int
id_type_annulation int
id_ville
*/

            prestation.IdPrestation = dr.GetInt32(dr.GetOrdinal("id_prestation"));
            prestation.Libelle = dr.GetString(dr.GetOrdinal("libelle_prestation"));
            prestation.Date = dr.GetDateTime(dr.GetOrdinal("date"));
            prestation.Duree = dr.GetString (dr.GetOrdinal("duree"));
            prestation.MasseurMin = dr.GetInt32(dr.GetOrdinal("masseur_min"));
            prestation.CoutMads = dr.GetInt32(dr.GetOrdinal("cout_mads"));
            prestation.DateCreation = dr.GetDateTime(dr.GetOrdinal("date_creation"));
            prestation.Adresse = dr.GetString(dr.GetOrdinal("adresse_prestation"));
            prestation.IdPartenaire = dr.GetInt32(dr.GetOrdinal("id_partenaire"));
            prestation.IdTypePrestation = dr.GetInt32(dr.GetOrdinal("id_type_prestation"));
            prestation.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            

            if (!dr.IsDBNull(dr.GetOrdinal("date_annulation")))                 // Date d'annulation peut être null
            {
                prestation.DateAnnulation = dr.GetDateTime(dr.GetOrdinal("date_annulation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("masseur_max")))                 // Date d'annulation peut être null
            {
                prestation.MasseurMax = dr.GetInt32(dr.GetOrdinal("masseur_max"));
            }
            


            return prestation;
        }

        public void ModifierPrestation(PrestationEntity prestation)
        {
            

            IDbCommand cmd = GetCommand(@"UPDATE prestation SET libelle_prestation = @libelle, 
                                              date = @date, 
                                              duree = @duree,
                                              masseur_min = @masseurMin,
                                              masseur_max = @masseurMax,
                                              cout_mads = @coutMads, 
                                              date_creation = @dateCreation,
                                              adresse_prestation = @adressePrestation, 
                                              id_partenaire = @idPartenaire, 
                                              id_type_prestation = @idTypePrestation, 
                                              id_ville = @idVille 
                                              WHERE id_prestation = @id;");

            cmd.Parameters.Add(new MySqlParameter("@libelle", prestation.Libelle));
            cmd.Parameters.Add(new MySqlParameter("@date", prestation.Date));
            cmd.Parameters.Add(new MySqlParameter("@duree", prestation.Duree));
            cmd.Parameters.Add(new MySqlParameter("@masseurMin", prestation.MasseurMin));
            cmd.Parameters.Add(new MySqlParameter("@masseurMax", prestation.MasseurMax));
            cmd.Parameters.Add(new MySqlParameter("@coutMads", prestation.CoutMads));
            cmd.Parameters.Add(new MySqlParameter("@dateCreation", prestation.DateCreation));
            cmd.Parameters.Add(new MySqlParameter("@adressePrestation", prestation.Adresse));
            cmd.Parameters.Add(new MySqlParameter("@idPartenaire", prestation.IdPartenaire));
            cmd.Parameters.Add(new MySqlParameter("@idTypePrestation", prestation.IdTypePrestation));
            cmd.Parameters.Add(new MySqlParameter("@idVille", prestation.IdVille));
            cmd.Parameters.Add(new MySqlParameter("@id", prestation.IdPrestation));

            try
            {
                cmd.Connection.Open();
                prestation.IdPrestation = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception exc)
            {
                throw exc;
            }
          
        }

        //DataTable pour remonter les infos vers le détail partenaire
        public DataTable GetAllPrestaForPartners(int id)
        {
            DataTable prestaTable = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.prestation 
                                                   JOIN nomads.type_prestation ON prestation.id_type_prestation = type_prestation.id_type_prestation
                                                   JOIN nomads.partenaire ON  prestation.id_partenaire = partenaire.id_partenaire 
                                                   JOIN nomads.ville ON prestation.id_ville = ville.id_ville 
                                                   WHERE partenaire.id_partenaire = @id and prestation.date >= sysdate();
                                        ");

            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter();
                da.SelectCommand = (MySqlCommand)cmd;
                da.Fill(prestaTable);
            }
            catch (Exception exc)
            {

                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return prestaTable;
        }
        public object GetPastPrestaForPartners(int id)
        {
            DataTable prestaTable = new DataTable();

            IDbCommand cmd = GetCommand(@"SELECT * FROM nomads.prestation 
                                                   JOIN nomads.type_prestation ON prestation.id_type_prestation = type_prestation.id_type_prestation
                                                   JOIN nomads.partenaire ON  prestation.id_partenaire = partenaire.id_partenaire 
                                                   JOIN nomads.ville ON prestation.id_ville = ville.id_ville 
                                                   WHERE partenaire.id_partenaire = @id and prestation.date < sysdate();
                                        ");

            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                cmd.Connection.Open();

                MySqlDataAdapter da = new MySqlDataAdapter();
                da.SelectCommand = (MySqlCommand)cmd;
                da.Fill(prestaTable);
            }
            catch (Exception exc)
            {

                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return prestaTable;
        }

        public void SoustraireMads (MasseurEntity masseur)
        {


            IDbCommand cmd = GetCommand(@"UPDATE masseur SET nombre_mads = @nbMads 
                                                      WHERE id_masseur = @id;");

            cmd.Parameters.Add(new MySqlParameter("@nbMads", masseur.NombreMads));
            cmd.Parameters.Add(new MySqlParameter("@id", masseur.IdMasseur));

            try
            {
                cmd.Connection.Open();
                masseur.IdMasseur = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }

        public void RembourserMads(MasseurEntity masseur)
        {


            IDbCommand cmd = GetCommand(@"UPDATE masseur SET nombre_mads = @nbMads 
                                                      WHERE id_masseur = @id;");

            cmd.Parameters.Add(new MySqlParameter("@nbMads", masseur.NombreMads));
            cmd.Parameters.Add(new MySqlParameter("@id", masseur.IdMasseur));

            try
            {
                cmd.Connection.Open();
                masseur.IdMasseur = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }


    }




}
