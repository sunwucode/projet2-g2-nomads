﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFCEPF.AI107.Nomads.Entities;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class TypePartenaireDAO : DAO
    {
        public List<TypePartenaireEntity> GetAllTypes()
        {
            List<TypePartenaireEntity> types = new List<TypePartenaireEntity>();

            IDbCommand cmd = GetCommand("SELECT * FROM type_partenaire");

            try
            {
                cmd.Connection.Open();

                IDataReader dr = cmd.ExecuteReader();

                // 4 - Traiter le résultat
                while (dr.Read())
                {
                    types.Add(DataReaderToEntity(dr));
                }

            }
            catch (Exception exc)
            {

                throw exc;
            }
            finally
            {
               cmd.Connection.Close();
            }

            return types;
        }

        private TypePartenaireEntity DataReaderToEntity(IDataReader dr)
        {
            TypePartenaireEntity type = new TypePartenaireEntity();
            type.IdTypePartenaire = dr.GetInt32(dr.GetOrdinal("id_type_partenaire")) ;
            type.Libelle = dr.GetString(dr.GetOrdinal("libelle_type_partenaire")) ;

            return type;
        }
    }
}
