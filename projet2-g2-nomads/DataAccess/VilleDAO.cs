﻿using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.DataAccess
{
    public class VilleDAO : DAO
    {
        public List<VilleEntity> GetAllVilles()
        {
            List<VilleEntity> result = new List<VilleEntity>();

            // 1 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * FROM ville");

            try
            {
                // 2 - Ouvrir la connection
                cmd.Connection.Open();

                // 3 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 4 - Traiter le résultat
                while (dr.Read())
                {
                    result.Add(DataReaderToEntity(dr));
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 5 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }


        public VilleEntity GetByIdVille(int id)
        {
            VilleEntity result = new VilleEntity();

            // 1 - Préparer une commande SQL
            IDbCommand cmd = GetCommand(@"SELECT * 
                                        FROM ville 
                                        WHERE id_ville = @id");

            cmd.Parameters.Add(new MySqlParameter("@id", id));

            try
            {
                // 2 - Ouvrir la connection
                cmd.Connection.Open();

                // 3 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 4 - Traiter le résultat
                while (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 5 - Fermer la connection
                cmd.Connection.Close();
            }
            return result;
        }

        private VilleEntity DataReaderToEntity(IDataReader dr)
        {
            VilleEntity ville = new VilleEntity();
            ville.IdVille = dr.GetInt32(dr.GetOrdinal("id_ville"));
            ville.Nom = dr.GetString(dr.GetOrdinal("libelle_ville"));
            ville.CodePostal = dr.GetString(dr.GetOrdinal("code_postal"));

            return ville;
        }

    }
}
