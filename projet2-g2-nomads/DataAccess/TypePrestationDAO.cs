﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFCEPF.AI107.Nomads.Entities;
using MySql.Data.MySqlClient;
using System.Data;


namespace AFCEPF.AI107.Nomads.DataAccess
{
   public class TypePrestationDAO : DAO
   {
        private TypePrestationEntity DataReaderToEntity(IDataReader dr)
        {
            TypePrestationEntity typePresta = new TypePrestationEntity();

            typePresta.IdTypePrestation = dr.GetInt32(dr.GetOrdinal("id_type_prestation"));
            typePresta.LibellePrestation = dr.GetString(dr.GetOrdinal("libelle_type_prestation"));

            return typePresta;
        }
        public List<TypePrestationEntity> GetAllTypePrestation()
        {


            List<TypePrestationEntity> result = new List<TypePrestationEntity>();
            // Preparer la command
            IDbCommand cmd = GetCommand(@"SELECT * FROM type_prestation");


            try
            {
                // ouvrir la connection 
                cmd.Connection.Open();

                // Exécuter la command
                IDataReader dr = cmd.ExecuteReader();

                // Traiter le resultat
                while (dr.Read())
                {

                    result.Add(DataReaderToEntity(dr));

                }


            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // fermer la connection 
                cmd.Connection.Close();

            }



            return result;
        }

    }
}
