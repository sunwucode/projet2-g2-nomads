﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class RefCompetenceEntity
    {
       

        public int IdCompetence { get; set; }
        public string Libelle { get; set; }


        public RefCompetenceEntity()
        {
           
        }
        public RefCompetenceEntity(int id, string competence)
        {
           IdCompetence = id;
            Libelle = competence;
        }

    }
}
