﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class InscriptionPrestationEntity
    {
        public int IdInscription { get; set; }
        public DateTime DatePrestation { get; set; }
        public DateTime DateInscription { get; set; }
        public DateTime? DateValidation { get; set; }
        public DateTime? DateDesistement { get; set; }
        public int CoutMads { get; set; }
        public string LibelleFbMasseur { get; set; }
        public DateTime? DateFbMasseur { get; set; }
        public double? MontantPrestaIntervention { get; set; }
        public DateTime? DateFacture { get; set; }
        public int? NoteFeedback { get; set; }
        public int? NombreClients { get; set; }
        public double? ChiffreAffaires { get; set; }
        public DateTime? DateReservation { get; set; }
        public DateTime? DateRetrait { get; set; }
        public DateTime? DateRetour { get; set; }
        public int? CoutMadsArrhes { get; set; }
        public int? TypePenalite { get; set; }
        public int? IdMateriel { get; set; }
        public int IdMasseur { get; set; }
        public int IdPrestation { get; set; }
    }
}
