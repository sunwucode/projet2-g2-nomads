﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
   public class JeuxDonneesAjoutPresta
    {

        public PrestationEntity DemoChampVide()
        {

            PrestationEntity demoTournee = new PrestationEntity();
            demoTournee.Libelle = "Demo champ vide";
            demoTournee.Date = DateTime.Now.AddDays(3);
            demoTournee.Duree = "05:30:00";
            demoTournee.Adresse = "9, Bv Ornano ";
           
            demoTournee.IdPartenaire = 1;
            demoTournee.DateCreation = DateTime.Now;
            demoTournee.IdTypePrestation = 1;
            demoTournee.IdVille = 1;
            demoTournee.MasseurMax = 4;
            return demoTournee;
        }

        public PrestationEntity DemoDateInvalide()
        {
            PrestationEntity demoTournee = new PrestationEntity();
            
            demoTournee.Libelle = "Demo date non valide";
            demoTournee.Date = DateTime.Now.AddDays(-3);
            demoTournee.Duree = "08:00:00";
            demoTournee.Adresse = "32, Grande rue Charles de Gaulle";
            demoTournee.MasseurMin = 4;
            demoTournee.CoutMads = 16;
            demoTournee.IdPartenaire = 4;
            demoTournee.DateCreation = DateTime.Now;
            demoTournee.IdTypePrestation = 2;
            demoTournee.IdVille = 2;
            demoTournee.MasseurMax = 6;
            return demoTournee;
        }

        public PrestationEntity DemoPrestaNbMasseurNonValide()
        {
            PrestationEntity demoTournee = new PrestationEntity();

            demoTournee.Libelle = "Demo Prestation";
            demoTournee.Date = DateTime.Now.AddDays(3);
            demoTournee.Duree = "08:00:00";
            demoTournee.Adresse = "32, Grande rue Charles de Gaulle";
            demoTournee.MasseurMin = 8;
            demoTournee.CoutMads = 48;
            demoTournee.IdPartenaire = 4;
            demoTournee.DateCreation = DateTime.Now;
            demoTournee.IdTypePrestation = 2;
            demoTournee.IdVille = 2;
            demoTournee.MasseurMax = 4;


            return demoTournee;
        }

    }
}
