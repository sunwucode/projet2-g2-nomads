﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class TypeAnnulationEntity
    {
        public int IdTypeAnnulation { get; set; }
        public string RaisonAnnulation { get; set; }

    }
}
