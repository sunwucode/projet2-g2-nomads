﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Nomads.Entities
{
    public class SuiviEtatEntity
    {
        public DateTime DateVerification { get; set; }
        public int? IdMateriel { get; set; }
        public int? IdEtatMateriel { get; set; }

    }
}
