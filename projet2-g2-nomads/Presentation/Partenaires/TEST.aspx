﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="TEST.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Partenaires.TEST" %>

<%@ Register Src="~/Masseurs/UCRechecheMasseurs.ascx" TagPrefix="uc1" TagName="UCRechecheMasseurs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
  
    
     <div class="recherche">  <!-- la recherche idéale Nom / ville / compétence / sexe  -->
        Nom du Masseur : <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>         
        Prénom:<asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox>
        Compétence : <asp:DropDownList ID="ddlComp" runat="server"></asp:DropDownList>
        sexe : <asp:DropDownList ID="ddlSexe" runat="server"></asp:DropDownList>
        Ville : <asp:DropDownList   ID="ddlVille" runat="server"></asp:DropDownList> 
         
        
       <asp:Button ID="btnSearchMasseurs" runat="server" OnClick="btnSearchMasseurs_Click" Text="Chercher des masseurs compatibles" />

            </div>
    
    <div class="grid-liste">

    <div class="masseur liste-item">       
       
            <asp:GridView ID="gvMasseurs" AutoGenerateColumns="false" runat="server">               
               
                    <Columns>
                        <asp:BoundField DataField="nom" HeaderText="Nom" />
                        <asp:BoundField DataField="prenom" HeaderText="Prénom" />
                        <asp:BoundField DataField="libelle_competence" HeaderText="Compétence" />
                        <asp:BoundField DataField="libelle_sexe" HeaderText="Sexe" />
                        <asp:BoundField DataField="libelle_ville" HeaderText="Ville" />                  

                     </Columns>
                </asp:GridView>
              
        </div>

        </div>
   

</asp:Content>
