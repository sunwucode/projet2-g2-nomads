﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;

namespace AFCEPF.AI107.Nomads.Presentation.Partenaires
{
    public partial class Partenaire : System.Web.UI.Page
    {
        private int id_partenaire;
        protected void Page_Load(object sender, EventArgs e)
        {
            // récupération de l'id du partenaire :
            int.TryParse(Request["id"], out id_partenaire);

            

            if (!IsPostBack) // si le partenaire est affiché pour la première fois
            {
                // récupération de l'entité partenaire
                PartenaireBU bu = new PartenaireBU();
                PartenaireEntity partenaire = bu.GetPartenaire(id_partenaire);

                gvPresta.DataSource = bu.GetPartnerPrestations(id_partenaire);
                gvPresta.DataMember = "id_partenaire"; //QU EST CE QUE CA FAIT ???
                gvPresta.DataBind();

                gvHistorique.DataSource = bu.GetHistoriquePartnerPrestations (id_partenaire);
                gvHistorique.DataBind();

                //DataTable infos = bu.GetDetailsPartner();

                //lien entre donnée et prés pour affichage
                if (partenaire != null)
                {
                    lblDeno.Text = partenaire.DenominationSociale;
                    
                    lblDateInscription.Text = partenaire.DateInscription.ToString();
                    // ligne originale lblType.Text = partenaire.IdTypePartenaire.ToString();  renvoie int
                    lblType.Text = partenaire.TypePartenaire;

                    lblVille.Text = partenaire.NomVille;
                    lblAdresse.Text = partenaire.Adresse;
                    lblDateInscription.Text = partenaire.DateInscription.ToString("yyyy-MM-dd");
                    lblDatePartenariat.Text = partenaire.DebutPartenariat.ToString("yyyy-MM-dd");

                    //tableau
                    lblNom.Text = partenaire.NomContact;
                    lblMailPrincipal.Text = partenaire.MailContact;
                    lblNuméro.Text = partenaire.NumeroContact;
                    lblNomSecondaire.Text = partenaire.NomSecondaire;
                    lblMailSecondaire.Text = partenaire.MailSecondaire;


                }

            }
        }

        protected void btnSupprimer_Click(object sender, EventArgs e)
        {
            try
            {
                PartenaireBU bu = new PartenaireBU();
                bu.SupPartenaire(id_partenaire);
                lblMessage.CssClass = "success";
                lblMessage.Visible = true;
                lblMessage.Text = "Partenaire Supprimé";
                Response.AddHeader("REFRESH", "2;URL=/Partenaires/ListePartenaire.aspx");
            }
            catch (Exception exp)
            {
                lblMessage.CssClass = "error";
                lblMessage.Text = exp.Message;
                lblMessage.Visible = true;

            }


        }

       
    } 
}