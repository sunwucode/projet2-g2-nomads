﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="AjoutPartenaire.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Partenaires.AjoutPartenaire" %>

<%@ Register Src="~/Partenaires/UCToggleBarPartenaire.ascx" TagPrefix="uc1" TagName="UCToggleBarPartenaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarPartenaire runat="server" ID="UCToggleBarPartenaire" />
    <div class="champs">
    <asp:DropDownList ID="ddlDemoAjoutPartenaire" runat="server"> </asp:DropDownList>
     <asp:Button ID="Demo" runat="server" Text="Valider" OnClick="btnDemo_Click"/>
    </div>
    <h2>Partenaire</h2>

        <div class="masseur-item">

         <table>
                 <tr>
                    <td class="label">Dénomination :</td>
                    <td><asp:TextBox ID="txtDeno" runat="server"></asp:TextBox> </td>
                 </tr>

                <tr>
                    <td class="label">Adresse :   </td>
                    <td><asp:TextBox ID="txtAdresse" runat="server"></asp:TextBox></td>
                </tr>

                <tr>
                    <td class="label">Date de début du partenariat :   </td>
                    <td><asp:TextBox ID="txtDebutPartenariat" TextMode="Date" runat="server"></asp:TextBox></td> 
                </tr>

                <tr>
                    <td class="label">Ville : </td>
                    <td><asp:DropDownList ID="ddlVille" runat="server"></asp:DropDownList></td>
                </tr>

                <tr>
                    <td class="label">Type partenaire :</td>
                    <td><asp:DropDownList ID="ddlPartenaire" runat="server"></asp:DropDownList></td>
                </tr>

                <tr><h3> Infos contact : </h3></tr>

                <tr> 
                    <td class="label">Contact principal : </td>
                    <td><asp:TextBox ID="txtContact" runat="server"></asp:TextBox></td>  
                </tr>

                <tr>
                    <td class="label">Email :  </td>
                    <td><asp:TextBox ID="txtmail" runat="server"></asp:TextBox></td>
                </tr>

                <tr> 
                    <td class="label">Numéro : </td>
                    <td><asp:TextBox ID="txtNumero" runat="server"></asp:TextBox></td>
                </tr>

                <tr> 
                    <td class="label">Contact Secondaire : </td>
                    <td><asp:TextBox ID="txtContactSecondaire" runat="server"></asp:TextBox></td>  
                </tr>

                <tr>
                    <td class="label">Email :  </td>
                    <td><asp:TextBox ID="txtMailSecondaire" runat="server"></asp:TextBox></td>
                </tr>
    
                <tr> 
                    <td></td>
                    <td>
                        <asp:Button ID="btnAjout" runat="server" OnClick="btnAjout_Click" Text="Ajouter Partenaire" />
                        <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" OnClick="btnAnnuler_Click"/>
                    </td>
                </tr>
         </table>

    </div>
    <asp:Label ID="lblMessage" Visible="false" runat="server" Text=""></asp:Label>
</asp:Content>
