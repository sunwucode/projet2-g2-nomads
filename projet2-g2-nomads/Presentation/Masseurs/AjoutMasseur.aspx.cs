﻿using AFCEPF.AI107.Nomads.Business;
using AFCEPF.AI107.Nomads.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AFCEPF.AI107.Nomads.Presentation
{
    public partial class AjoutMasseur : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SexeBU sexeBu = new SexeBU();
                List<SexeEntity> sexes = sexeBu.GetListSexe();

                VilleBU villeBu = new VilleBU();
                List<VilleEntity> villes = villeBu.GetListVille();

                ddlSexe.DataTextField = "Libelle";
                ddlSexe.DataValueField = "IdSexe";
                ddlSexe.DataSource = sexes;
                ddlSexe.DataBind();

                ddlVille.DataTextField = "Nom";
                ddlVille.DataValueField = "IdVille";
                ddlVille.DataSource = villes;
                ddlVille.DataBind();

                JeuxDonneesAjoutMasseur donneeDemo = new JeuxDonneesAjoutMasseur();
                MasseurEntity masseurHomme = new MasseurEntity();
                MasseurEntity masseurFemme = new MasseurEntity();
                masseurHomme = donneeDemo.DemoMasseurHomme();
                masseurFemme = donneeDemo.DemoMasseurFemme();
                ddlDemoAjoutMasseur.Items.Add(masseurHomme.Prenom);
                ddlDemoAjoutMasseur.Items.Add(masseurFemme.Prenom);

            }

        }

        protected void btnAjout_Click(object sender, EventArgs e)
        {
            MasseurEntity masseur = new MasseurEntity();
            masseur.Nom = txtNom.Text.ToUpper();
            masseur.Prenom = toCamelCase(txtPrenom.Text);
            masseur.Adresse = txtAdress.Text;
            
            masseur.NumeroTelephone = txtTelephone.Text;        // Trouver une procédure de vérification du format
            masseur.IdSexe = int.Parse(ddlSexe.SelectedValue);
            masseur.IdVille = int.Parse(ddlVille.SelectedValue);
            masseur.Inscription = DateTime.Now;
            masseur.DateNaissance = DateTime.Parse(txtDateNaissance.Text);


            try
            {
                MasseursBU bu = new MasseursBU();
                bu.AjouterMasseur(masseur);
                lblMessage.Text = "Le masseur a bien été ajouté!";
                lblMessage.Visible = true;
                lblMessage.CssClass = "success";
                Response.AddHeader("REFRESH", "2;URL=/Masseurs/ListeMasseurs.aspx");
            }
            catch (Exception exp)
            {
                lblMessage.Text = exp.Message;
                lblMessage.CssClass = "error";
                lblMessage.Visible = true;
            }
            
            

            // Redirection 
        }

        protected string toCamelCase(string str)
        {
            string firtsLetter = str.Substring(0, 1).ToUpper();
            string nextLetters = str.Substring(1).ToLower();
            string camelCase = firtsLetter + nextLetters; 
            return camelCase.Trim();
        }
        protected void btnDemo_Click(object sender, EventArgs e)
        {
            JeuxDonneesAjoutMasseur donneeDemo = new JeuxDonneesAjoutMasseur();
            MasseurEntity masseurHomme = new MasseurEntity();
            MasseurEntity masseurFemme = new MasseurEntity();
            masseurHomme = donneeDemo.DemoMasseurHomme();
            masseurFemme = donneeDemo.DemoMasseurFemme();
            string masseurChoisi = ddlDemoAjoutMasseur.Text;
            if(masseurChoisi==masseurFemme.Prenom)
            {
                txtNom.Text             = masseurFemme.Nom;
                txtPrenom.Text          = masseurFemme.Prenom;
                txtAdress.Text          = masseurFemme.Adresse;
                txtTelephone.Text       = masseurFemme.NumeroTelephone;
                ddlSexe.SelectedValue   = masseurFemme.IdSexe.ToString();
                ddlVille.SelectedValue  = masseurFemme.IdVille.ToString();
                txtDateNaissance.Text   = masseurFemme.DateNaissance.ToString("yyyy-MM-dd");
            }
            if (masseurChoisi == masseurHomme.Prenom)
            {
                txtNom.Text             = masseurHomme.Nom;
                txtPrenom.Text          = masseurHomme.Prenom;
                txtAdress.Text          = masseurHomme.Adresse;
                txtTelephone.Text       = masseurHomme.NumeroTelephone;
                ddlSexe.SelectedValue   = masseurHomme.IdSexe.ToString();
                ddlVille.SelectedValue  = masseurHomme.IdVille.ToString();
                txtDateNaissance.Text   = masseurHomme.DateNaissance.ToString("yyyy-MM-dd");
            }
        }

    }
}