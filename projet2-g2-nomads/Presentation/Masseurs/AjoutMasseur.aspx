﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="AjoutMasseur.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.AjoutMasseur" %>

<%@ Register Src="~/Masseurs/UCToggleBarMasseur.ascx" TagPrefix="uc1" TagName="UCToggleBarMasseur" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarMasseur runat="server" id="UCToggleBarMasseur" />
     <div class="champs">
    <asp:DropDownList ID="ddlDemoAjoutMasseur" runat="server"> </asp:DropDownList>
     <asp:Button ID="Demo" runat="server" Text="Valider" OnClick="btnDemo_Click"/>
    </div>

        <h1>Ajouter un Masseur</h1>

    <div class="masseur-item">
        <table>
                 <tr>
                    <td class="label">Nom :</td>
                    <td><asp:TextBox ID="txtNom" runat="server"></asp:TextBox></td>
                 </tr>

                <tr>
                    <td class="label">Prenom :</td>
                    <td><asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox></td>
                </tr>

                <tr>
                    <td class="label">Sexe :</td>
                    <td><asp:DropDownList ID="ddlSexe" runat="server"></asp:DropDownList></td> 
                </tr>

                <tr>
                    <td class="label">Date de Naissance :</td>
                    <td><asp:TextBox ID="txtDateNaissance" runat="server" TextMode="date"></asp:TextBox></td>
                </tr>

                <tr>
                    <td class="label">Téléphone :</td>
                    <td><asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox></td>
                </tr>

                <tr> 
                    <td class="label">Adresse : </td>
                    <td><asp:TextBox ID="txtAdress" runat="server"></asp:TextBox></td>  
                </tr>

                <tr>
                    <td class="label">Ville:</td>
                    <td><asp:DropDownList ID="ddlVille" runat="server"></asp:DropDownList></td>
                </tr>
            
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnAjout" runat="server" Text="Enregistrer" OnClick="btnAjout_Click" />
                    </td>
                    <td><asp:Label CssClass="error" ID="lblMessage" Visible="false" runat="server" Text=""></asp:Label></td>
                </tr>
         </table>

    </div>
    
</asp:Content>
