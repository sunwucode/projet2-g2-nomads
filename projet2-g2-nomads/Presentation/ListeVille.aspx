﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="ListeVille.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.ListeVille" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h1>Liste des Villes</h1>
    <asp:Repeater ID="rptVilles" runat="server">
        <ItemTemplate>
            <h5><%# DataBinder.Eval(Container.DataItem, "IdVille") %></h5>
            <p>Ville: <%# DataBinder.Eval(Container.DataItem, "Nom") %>  <%# DataBinder.Eval(Container.DataItem, "CodePostal") %></p>

        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
