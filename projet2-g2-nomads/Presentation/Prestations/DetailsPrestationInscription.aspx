﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="DetailsPrestationInscription.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.Tournees.DetailsPrestationInscription" %>

<%@ Register Src="~/Prestations/UCToggleBarPrestation.ascx" TagPrefix="uc1" TagName="UCToggleBarPrestation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UCToggleBarPrestation runat="server" ID="UCToggleBarPrestation" />
     
        <br />
        <asp:Label class="inscription" ID="inscription" runat="server" Text="Label">Vous inscrivez : <strong><%=Request["prenom"]%><%=Request["nom"]%></strong></asp:Label>

    <div class="grid-liste">

         <!--Table Détails Prestation-->
         <div class="masseur liste-item">
             <h2><asp:Label ID="lblLibelle" runat="server"></asp:Label></h2>
             <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>

             <table>
                    <tr>
                        <tr>
                        <td>Date :</td>
                        <td><asp:Label ID="lblDate" runat="server" ></asp:Label></td>
                    </tr>
                     <tr>
                        <td>Durée :</td>
                        <td><asp:Label ID="lblDuree" runat="server" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Nombre de masseurs minimum :</td>
                        <td><asp:Label ID="lblMasseurMin" runat="server" ></asp:Label></td>
                    </tr>
                  <tr>
                        <td>Nombre de masseurs maximum :</td>
                        <td><asp:Label ID="lblMasseurMax" runat="server" ></asp:Label></td>
                    </tr>
                     <tr>
                        <td>Adresse :</td>
                        <td><asp:Label ID="lblAdresse" runat="server"></asp:Label></td>
                    </tr>
                     <tr>
                        <td>Mads nécessaires pour s'inscrire à la prestation :</td>
                        <td><h3><asp:Label ID="lblMads" runat="server" ></asp:Label></h3></td>
                    </tr>
                   
                      <tr>
                        <td>Particularités de la prestation : </td>
                        <td><asp:Label ID="lblParticularites" runat="server" ><strong>à créer dans la BDD!!!</strong></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="label""></td>
                        <td>
                            <br />
                            <asp:Button ID="btnInscpriptionPresta" runat="server" Text="Inscription" onclick="btnInscpriptionPresta_Click"/>
                        </td>
                    </tr>
                </table>  
         </div>

        <!--GridView Masseurs Inscrits-->
        <div class= "masseur liste-item">

                  <h2><asp:Label ID="Label1" runat="server"></asp:Label>Inscriptions : </h2>

                  <asp:Label CssClass="error" ID="lblMessage" runat="server" Visible="false"></asp:Label>

                <asp:GridView  ID="gvMasseursInscrits" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gvMasseursInscrits_RowCommand" AutoGenerateColumns="false">
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>

                    <EditRowStyle BackColor="#b9ebc3"></EditRowStyle>

                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

                    <HeaderStyle BackColor="#339966" Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <PagerStyle HorizontalAlign="Center" BackColor="#F0F8FF" ForeColor="White"></PagerStyle>

                    <RowStyle BackColor="#ebf9ee"></RowStyle>

                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

                    <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>

                    <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>

                    <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>

                    <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                
                    <Columns>
                        <asp:BoundField DataField="Nom" HeaderText="Nom" />
                        <asp:BoundField DataField="Prenom" HeaderText="Prénom" />
                        <asp:BoundField DataField="Inscription" HeaderText="Date d'Inscription" />
                         <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDesinscriptionMasseur" runat="server" 
                                    CommandName="btnDesinscriptionPrestations_OnClick" 
                                    CommandArgument='<%# Bind("IdMasseur")%>' 
                                    Text="Désinscription" />
                            </ItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
    </div>
</asp:Content>
