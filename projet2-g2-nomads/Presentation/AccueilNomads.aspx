﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="AccueilNomads.aspx.cs" Inherits="AFCEPF.AI107.Nomads.Presentation.AccueilNomads" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1> Bonjour,&nbsp Alexandra !</h1>
    <br />
    <br />
    <nav class="dashboard">  
            
            <div class="round-icon" id="masseurs">
                <img class="icons" src="https://img.icons8.com/pastel-glyph/64/000000/massage.png"/>        
                <a class="dashboard-link" href="/Masseurs/ListeMasseurs.aspx">Liste Masseurs</a>
            </div>
                
                 <div class="round-icon" id="partenaires">
                     <img class="icons" src="https://img.icons8.com/ios/50/000000/handshake-heart.png"/>
                <a class="dashboard-link" href="/Partenaires/ListePartenaire.aspx">Liste Partenaires</a>
                 </div>   
                
                <div class="round-icon" id="prestations">
                    <img class="icons" src="https://img.icons8.com/ios/50/000000/cocktail.png"/>
                <a class="dashboard-link" href="/Prestations/ListePrestations.aspx">Liste Prestations</a>
                </div>
                
                <div class="round-icon" id="mads">
                    <img class="icons" src="https://img.icons8.com/material-outlined/48/000000/us-dollar.png"/>
                    <a href="/PageEnConstruction.aspx">Mads</a>
                </div>
                <div class="round-icon" id="matos">
                    <img class="icons" src="https://img.icons8.com/ios/50/000000/alluminium-massagetable.png"/>
                    <a href="/PageEnConstruction.aspx">Matériel</a>
                </div>
                <div class="round-icon" id="notif">
                    <img class="icons" src="https://img.icons8.com/ios/50/000000/construction-mail-open.png"/>
                    <a href="/PageEnConstruction.aspx">Notifications</a>
                </div>
                <a></a>
                <a></a>

                <!-- Supprimés Le but c'est qu'elle accède au sous DS des grandes rubriques ou à une page.
                    <a href="/Partenaires/AjoutPartenaire.aspx">Ajouter un Partenaires</a>
                    <a href="/Masseurs/AjoutMasseur.aspx">Inscription Masseur</a>
                    <a href="/Tournees/AjoutTournee.aspx">Ajouter une Tournée</a>
                    -->
            </nav>
   
       
</asp:Content>
